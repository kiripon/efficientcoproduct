{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE OverlappingInstances #-}
module Data.Coproduct
       ( module C
       , flatType
       )where

import Data.Coproduct.Type as C
import Data.Coproduct.Class as C
import TH.Constructor (flatType)
