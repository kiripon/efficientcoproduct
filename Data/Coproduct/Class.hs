{-# LANGUAGE TypeOperators#-}
{-# LANGUAGE MultiParamTypeClasses#-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverlappingInstances #-}
module Data.Coproduct.Class where

class (Functor sub,Functor sup) => sub :<: sup where
  inj :: sub a -> sup a

instance Functor f => f :<: f where
  inj = id
  {-# INLINE inj #-}
