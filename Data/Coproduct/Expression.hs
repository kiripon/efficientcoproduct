{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE UndecidableInstances  #-}
module Data.Coproduct.Expression
       ( inject
       , val
       , (.+.)
       , (.*.)
       , fold
       , eval
       , evalAlgebra
       , Add(..)
       , Val(..)
       , Mul(..)
       , Expr(..)
       )where

import Data.Coproduct
data Add e = Add e e deriving Functor
data Val e = Val Int deriving Functor
data Mul e = Mul e e deriving Functor

inject :: (Functor f,Functor g,f :<: g) => f (Fix g) -> Fix g
inject = Fix . inj
{-# INLINE inject #-}

val :: (Val :<: f,Functor f) => Int -> Fix f
val = inject . Val
{-# INLINE val #-}

infixl 6 .+.
(.+.) :: (Add :<: f, Functor f) => Fix f -> Fix f -> Fix f
a .+. b = inject $ Add a b
{-# INLINE (.+.) #-}

infixl 7 .*.
(.*.) :: (Mul :<: f, Functor f) => Fix f -> Fix f -> Fix f
a .*. b = inject $ Mul a b
{-# INLINE (.*.) #-}

instance Mul :<: Expr where
  inj = \(Mul x1 x2) -> MulE x1 x2
  {-# INLINE inj #-}
instance Add :<: Expr where
  inj = \(Add x1 x2) -> AddE x1 x2
  {-# INLINE inj #-}
instance Val :<: Expr where
  inj = \(Val i) -> ValE i
  {-# INLINE inj #-}

-- isomorphism
class Iso f g where
  iso :: f a -> g a
  riso :: g a -> f a

instance Iso f g => Iso g f where
  iso = riso
  riso = iso

class Eval f where
  evalAlgebra :: f Int -> Int
  
instance (Eval a,Eval b)
         => Eval (a :+: b)where
  evalAlgebra (InL a) = evalAlgebra a
  evalAlgebra (InR b) = evalAlgebra b
  {-# INLINE evalAlgebra #-}

instance Eval Add where
  evalAlgebra (Add e1 e2) = e1 + e2
  {-# SPECIALIZE INLINE evalAlgebra :: Add Int -> Int#-}
  
instance Eval Mul where
  evalAlgebra (Mul e1 e2) = e1 * e2
  {-# SPECIALIZE INLINE evalAlgebra :: Mul Int -> Int#-}
instance Eval Val where
  evalAlgebra (Val i) = i
  {-# SPECIALIZE INLINE evalAlgebra :: Val Int -> Int#-}

instance Eval Expr where
  evalAlgebra = \x -> case x of
    AddE x1 x2 -> evalAlgebra (Add x1 x2)
    MulE x1 x2 -> evalAlgebra (Mul x1 x2)
    ValE i     -> evalAlgebra (Val i)
  {-# INLINE evalAlgebra #-}
    
data Expr e = ValE Int
            | MulE e e
            | AddE e e
            deriving(Functor)

fold :: Functor f => (f c -> c) -> Fix f -> c
fold = \f ->  f . fmap (fold f) . out
{-# INLINE fold #-}

eval :: (Eval f, Functor f) => Fix f -> Int
eval = fold evalAlgebra
{-# INLINE eval #-}
