{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor#-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE FlexibleContexts#-}
module Data.Coproduct.Type
       (
         (:+:),
         caseF,
         Fix(..)
       )where

import Data.Coproduct.Class as C

-- Coproduct of Functor

infixr 6 :+:
-- | Coproduct
data (f :+: g) e = InR (g e) 
                 | InL (f e)
                 deriving (Functor)

instance (Show (f a),Show (g a)) => Show ((f :+: g) a) where
  show (InL v) = show v
  show (InR v) = show v

caseF :: (f a -> b) -> (g a -> b) -> (f :+: g) a -> b
caseF f g x = case x of
  InL a -> f a
  InR a -> g a

instance (Functor f,Functor g,Functor h,f :<: g)　=> f :<: (h :+: g) where
  inj = InR . inj
  {-# INLINE inj #-}

instance (Functor f,Functor g) 
         => f :<: (f :+: g) where
  inj = InL
  {-# INLINE inj #-}
  
newtype Fix f = Fix {out::f (Fix f)}
