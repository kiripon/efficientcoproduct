module Data.Free where

import Control.Applicative

data Free f a = Free (f (Free f a))
              | Pure a

instance (Functor f) => Functor (Free f) where
  {-# INLINE fmap #-}
  fmap f (Pure a) = Pure (f a)
  fmap f (Free x) = Free (fmap (fmap f) x)

instance Functor f => Applicative (Free f) where
  pure = Pure
  {-# INLINE pure #-}
  Pure a <*> f = fmap a f
  Free a <*> f = Free (fmap (<*> f) a)
  {-# INLINE (<*>)#-}

instance (Functor f) => Monad (Free f) where
  return = Pure
  {-# INLINE return #-}
  Pure a >>= f = f a
  Free a >>= f = Free $ fmap (>>= f) a
  {-# INLINE (>>=) #-}
