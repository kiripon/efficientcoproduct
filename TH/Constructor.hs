{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
module TH.Constructor
       (flatType
       ,makeConstructors
       )where
import Language.Haskell.TH
import Control.Applicative
import Control.Monad (when)
import Data.Coproduct.Type ((:+:))
import Data.List (foldl',find)

-- |usage: write
-- 
-- @
--  ...
--  type Fexpr = Add :+: Mul :+: Val
--  flatType 'Fexpr
--  ...
-- @
-- 
-- then automatically derived data declarations below:
-- 
-- @
-- data FexprFlatten a = FexprAdd a
--                     | FexprMul a
--                     | FexprVal a
-- @

flatType :: Name -> Q [Dec]
flatType givenTypeName = do
  r <- reify givenTypeName
  case r of
   TyConI (TySynD
           name       -- type name
           tyvarbndrs -- binding type variables
           trueType   -- real type
          ) -> flatType' name tyvarbndrs trueType
   _ -> fail $ "error: flatType: typename" ++ nameBase givenTypeName ++ "is not type synonym."

flatType' :: Name -> [TyVarBndr] -> Type -> Q [Dec]
flatType' name tyvarbndrs trueType = do
  --runIO $ putStrLn $ "tyvarbndrs:" ++ show tyvarbndrs
  d <- dataDec
  runIO $ print . ppr $ d
  fmap (:[]) $ dataDec
  where
    fixTV = PlainTV (mkName "a")
    tyvarbndrs' = tyvarbndrs
    tyVarAssocList = zip tyvarbndrs' renamedTyVarBndrs
    renamedTyVarBndrs = map rename tyvarbndrs'
    prefix = "hoge"
    rename (PlainTV a) = PlainTV (mkName $ prefix ++ nameBase a)
    rename (KindedTV a k) = KindedTV (mkName $ prefix ++ nameBase a) k
    dataDec = do
      DataD [] (mkName $ nameBase name ++ "Flatten")
        (renamedTyVarBndrs ++ [fixTV]) -- tyVar bindings
        <$> makeConstructors name fixTV tyVarAssocList trueType -- constructors
        <*> return [mkName "Functor"] -- deriving instance

-- | makes constructor by name and replace type variables.
makeConstructors :: Name -> TyVarBndr -> [(TyVarBndr,TyVarBndr)] -> Type -> Q [Con]
makeConstructors prefix fixTV tyvarassocs truetype = do
  runIO $ print tyvarassocs
  concat <$> mapM makeConstructor types
  where
    types = decompCoproduct (ConT ''(:+:))  truetype
    makeConstructor (ConT tyName) = do
      TyConI (DataD _ _ tvbndr cons _) <- reify tyName
      return $ map (mkCon tvbndr) cons
    makeConstructor _ = error "hoge"
    mkCon :: [TyVarBndr] -> Con -> Con
    mkCon tvbndrs (NormalC conName strictTypes) =
      NormalC (mkName $ nameBase prefix ++ nameBase conName) sTypes
      where
        functorTVreplace = (last tvbndrs,fixTV)
        sTypes = map f strictTypes
        f (strict,t@(VarT n))
          = let t' = case lookup (PlainTV n) (functorTVreplace:tyvarassocs) of
                       Nothing -> t
                       Just (PlainTV x) -> VarT x
                       Just (KindedTV x _k) -> VarT x
            in(strict,t')
        f t = t
    mkCon _ _ = undefined

decompCoproduct :: Type -> Type -> [Type]
decompCoproduct delim ts
  | op `AppT` t1 `AppT` t2 <- ts,
    op == delim           = t1 : decompCoproduct delim t2
  | otherwise             = [ts]

{-
data Hoge a = HogeFuga a | HogePiyo b
instance Fuga :<: Hoge where
  inj (Fuga x) = HogeFuga x

instance Piyo :<: Hoge where
  inj (Piyo x) = HogePiyo x
-}
assert :: Bool -> String -> Q ()
assert p msg = when p $ fail msg

{-
-- 与えられた型から導出できるinjectionをすべて見つける
-- ここで定義するよりconstructorの方で定義するのが良さそう
deriveInjection :: Name -> Q Dec
deriveInjection name = deriveInjectionWithType $ ConT name

deriveInjectionWithType :: Type -> Q Dec
deriveInjectionWithType typ@(ConT name') = do
  TyConI (DataD _ name _ cs _) <- reify name'
  undefined
-}

{-
  inj (Add x) = AddE x
-}
-- インスタンス宣言の導出

makeInjectDec :: Type -> Type -> Q [Dec]
makeInjectDec sub@(ConT subName) sup@(ConT supName) = do
  TyConI (DataD _ _ _ subConstrs _) <- reify subName 
  TyConI (DataD _ _ _ supConstrs _) <- reify supName
  assert (length subConstrs /= 1) "makeInjectionDec: left type is not singleton type"
  return $ [InstanceD []
            (ConT (mkName ":<:") `AppT` sub `AppT` sup)
            [mkfundef (nameBase supName) subConstrs supConstrs,
             PragmaD (InlineP (mkName "inj") Inline FunLike AllPhases)]
           ]
  {-
    instance sub :<: sup where
      [mkfundef (namebase supName) subConstrs supConstrs]
      {-# INLINE .. #-}
  -}
  where
    injTarget prefix sub' sup' = case find (p prefix sub') sup' of
      Nothing -> error "makeInjectDec:cannot find injection target"
      Just x -> x
      where
        p prefix' x y = prefix' ++ nameBase x == nameBase y
    mkfundef prefix [subconstr] supConstrs'
      = FunD (mkName "inj") [cls (constrArity subconstr)]
      where
        lname = cToN subconstr
        rname = injTarget prefix (cToN subconstr) (map cToN supConstrs')
                --case <supconstr> of {NormalC n _ -> n;_ -> undefined}
        varNames n = map (mkName . (++) "a" . show) [1 .. n]
        cls n = Clause [lpat n] (body n) [] -- clausures 
        lpat n = ConP lname (map VarP $ varNames n) -- left patterns
        body n = NormalB $ foldl' AppE (ConE rname) $ map VarE $ varNames n -- function bodies
        cToN c = case c of {NormalC n _ -> n;_ -> undefined} -- 
    mkfundef _ _ _ = error "makeInjectDec: "
    {-
       inj {<ConT lname> x1 .. xn = <ConT rname> x1 .. xn}
p       lname,rname is a constructor.
    -}
constrArity :: Con -> Int
constrArity (NormalC _ c) = length c
constrArity (RecC _ c) = length c
constrArity (InfixC _ _ _) = 2
constrArity (ForallC _ _ c) = constrArity c
