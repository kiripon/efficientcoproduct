{-# LANGUAGE TemplateHaskell #-}
module TH.DeriveFunction where
import Language.Haskell.TH
import Control.Applicative
import TH.Constructor2
import Data.Coproduct.Type ((:+:))
import Data.List (nub,foldl')
import qualified Data.Map as Map

----Template-haskell-types---
nullCxt :: Cxt
nullCxt = []

type BaseConName = Name
type FlatConName = Name
type ConTable = Map.Map BaseConName (FlatConName,Arity) -- ^ データコンストラクタのテーブル

type ConTypeTable = Map.Map BaseConName (FlatConName,[Type])

lookupCon :: BaseConName -> ConTable -> Maybe FlatConName
lookupCon x m = case Map.lookup x m of
  Nothing -> Nothing
  Just v  -> Just (fst v)

lookupArity :: BaseConName -> ConTable -> Maybe Arity
lookupArity x m = case Map.lookup x m of
  Nothing -> Nothing
  Just v -> Just $ snd v
-----------------------------


-- | 関数のDerivingを行う。方針は以下のとおり
-- 1. インターフェース
-- @
-- type Expr = Add :+: Mul :+: Val
-- flatTypeByName ''Expr "ExprFlat"
-- deriveFunction ''Expr "ExprFlat" ['eval]
-- deriveClassDecs ''Expr "ExprFlat" ['Eval]
-- -- ここで関数を指定しなければならないのが微妙。
-- -- できれば使ってる関数は勝手に導出してほしい。
-- -- 型名からは関数定義を引いてくることができない
-- -- 使ってる関数を全部書くのはちょっと問題ありそう……
-- -- reifyInstances関数を使えば何とかなりそう?
-- -- :+: から
-- @
-- 2. 実装
--   * 関数名からクラス名を引いてくる
--   * クラス名と関数名からインスタンス宣言を導出する
--   * データコンストラクタが見えるのでパターンマッチ

-- | 関数定義の段階でなにかしらの型クラスに属させて、そこから情報を引っ張ってくるのはどうか。
-- 関数定義用の文法を追加するって話があった。あれで定義した時に型クラスのメンバにしてやって,
-- THの実行時に関数定義を引いてくるとか。

-- | クラス名を関数から引いてくる
classFromFunName :: Name -> Q ParentName
classFromFunName n = do
  ClassOpI _opName _t className _fixity <- reify n
  return className

classesFromFunNames :: [Name] -> Q [ParentName]
classesFromFunNames ns = nub <$> mapM classFromFunName ns

tyConsFromCoproductSynonym :: Name -> Q [Type]
tyConsFromCoproductSynonym tn = do
  TyConI (TySynD _n _tvs ty) <- reify tn
  return $ decompCoproduct (ConT ''(:+:)) ty

deriveFunction :: Name -> String -> [Name] -> Q [Dec]
deriveFunction typeName strNewName funNames = do
  classNames <- classesFromFunNames funNames
  tyCons <- tyConsFromCoproductSynonym typeName
  let newTypeName = mkName strNewName
  let newType = ConT newTypeName
  --let tyConTable = Map.fromList $ undefined
  mapM (deriveClassDecs newType funNames tyCons) classNames

deriveClassDecs t f tc = undefined

makeDataDec :: Name -> t -> [ConQ] -> [Name] -> DecQ
makeDataDec newTypeName assoc = dataD (return nullCxt) newTypeName tyVarBndrs
  where
    tyVarBndrs = undefined

-- | なんかあれそれからAlgebraのインスタンス宣言を書くやつ。
-- Evalとかのalgebraのクラスインスタンス宣言とか関数宣言とかをちゃんとやってくれる。
makeAlgebraInstanceDec :: Name -> ConTable -> Name -> Name -> Q InstanceDec
makeAlgebraInstanceDec className assoc newTypeName funName = do
  return $ InstanceD nullCxt (ConT className `AppT` ConT newTypeName) [funDef funName]
  where
    funDef fn = FunD fn [cls]
      where
        cls = Clause [VarP $ mkName "x"] (NormalB $ CaseE (VarE $ mkName "x") matches) []
        matches = map makeMatch $ Map.toAscList assoc :: [Match]
        makeMatch (baseName,(flatName,arity)) = Match pat body []
          where
            pat  = ConP flatName $ map VarP vars
            vars = map (mkName . ("a" ++)) . map show $ [1..arity]
            body = NormalB $ VarE fn `AppE` (foldl' (AppE) (ConE baseName) $ map VarE vars)
