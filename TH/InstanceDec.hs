module TH.InstanceDec where

import Language.Haskell.TH
import Control.Monad (when)
import Data.List (foldl',find)

{-
data Hoge a = HogeFuga a | HogePiyo b
instance Fuga :<: Hoge where
  inj (Fuga x) = HogeFuga x

instance Piyo :<: Hoge where
  inj (Piyo x) = HogePiyo x
-}
assert :: Bool -> String -> Q ()
assert p msg = when p $ fail msg

-- 与えられた型から導出できるinjectionをすべて見つける
-- ここで定義するよりconstructorの方で定義するのが良さそう
deriveInjection :: Name -> Q Dec
deriveInjection name = deriveInjectionWithType $ ConT name

deriveInjectionWithType :: Type -> Q Dec
deriveInjectionWithType typ@(ConT name') = do
  TyConI (DataD _ name _ cs _) <- reify name'
  undefined

{-
  inj (Add x) = AddE x
-}
-- インスタンス宣言の導出

makeInjectDec :: Type -> Type -> Q [Dec]
makeInjectDec sub@(ConT subName) sup@(ConT supName) = do
  TyConI (DataD _ _ _ subConstrs _) <- reify subName 
  TyConI (DataD _ _ _ supConstrs _) <- reify supName
  assert (length subConstrs /= 1) "makeInjectionDec: left type is not singleton type"
  return $ [InstanceD []
            (ConT (mkName ":<:") `AppT` sub `AppT` sup)
            [mkfundef (nameBase supName) subConstrs supConstrs,
             PragmaD (InlineP (mkName "inj") Inline FunLike AllPhases)]
           ]
  {-
    instance sub :<: sup where
      [mkfundef (namebase supName) subConstrs supConstrs]
      {-# INLINE .. #-}
  -}
  where
    injTarget prefix sub' sup' = case find (p prefix sub') sup' of
      Nothing -> error "makeInjectDec:cannot find injection target"
      Just x -> x
      where
        p prefix' x y = prefix' ++ nameBase x == nameBase y
    mkfundef prefix [subconstr] supConstrs'
      = FunD (mkName "inj") [cls (constrArity subconstr)]
      where
        lname = cToN subconstr
        rname = injTarget prefix (cToN subconstr) (map cToN supConstrs')
                --case <supconstr> of {NormalC n _ -> n;_ -> undefined}
        varNames n = map (mkName . (++) "a" . show) [1 .. n]
        cls n = Clause [lpat n] (body n) [] -- clausures 
        lpat n = ConP lname (map VarP $ varNames n) -- left patterns
        body n = NormalB $ foldl' AppE (ConE rname) $ map VarE $ varNames n -- function bodies
        cToN c = case c of {NormalC n _ -> n;_ -> undefined} -- 
    mkfundef _ _ _ = error "makeInjectDec: "
    {-
       inj {<ConT lname> x1 .. xn = <ConT rname> x1 .. xn}
       lname,rname is a constructor.
    -}
constrArity :: Con -> Int
constrArity (NormalC _ c) = length c
constrArity (RecC _ c) = length c
constrArity (InfixC _ _ _) = 2
constrArity (ForallC _ _ c) = constrArity c
