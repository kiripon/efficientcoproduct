{-# LANGUAGE TemplateHaskell ,MultiParamTypeClasses ,DeriveFunctor #-}
{-# LANGUAGE TypeOperators#-}
module TH.InstanceDecExample where
import TH.InstanceDec
import Language.Haskell.TH
import Data.Coproduct
import TH.Constructor
import Data.Coproduct.Expression
data Hoge a = Hoge a deriving Functor
data Fuga a = FugaHoge a | FugaPiyo a a deriving Functor

makeInjectDec (ConT ''Hoge) (ConT ''Fuga)

{-
data Hoge a = Hoge a deriving Functor
data Fuga a = FugaHoge a | FugaPiyo a a

makeInjectDec (ConT ''Hoge) (ConT ''Fuga)
で以下が生成された
instance :<: Ghci3.Hoge Ghci2.Fuga
    where inj (Ghci3.Hoge a1) = Ghci2.FugaHoge a1
          {-# INLINE inj #-}

-}

type Piyo = Add :+: Mul :+: Val

flatType ''Piyo
