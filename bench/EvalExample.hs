{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts#-}
{-# LANGUAGE FlexibleInstances #-}
module Main where
import Data.Coproduct
import Data.Coproduct.Expression
import System.CPUTime
import Control.DeepSeq

main :: IO ()
main = do
  t1' <- getCPUTime
  let !x' = e :: Fix (Val :+: Mul :+: Add) --deepseqで書きなおす
  seq x' (return ())
  t2' <- getCPUTime
  print $ fold evalAlgebra x' 
  t3' <- getCPUTime
  putStrLn $ "construct  time:" ++ show (t2' - t1')
  putStrLn $ "evaluation time:" ++ show (t3' - t2')
  return ()

  t1 <- getCPUTime
  let !x = e :: Fix Expr
  t2 <- getCPUTime
  print $ fold evalAlgebra x
  t3 <- getCPUTime
  putStrLn $ "construct  time:" ++ show (t2 - t1)
  putStrLn $ "evaluation time:" ++ show (t3 - t2)
  

e2 :: (Add :<: f,Mul :<: f,Val :<: f) => Fix f
e2 = e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e .*.e .*. e .+. e .*. e

e :: (Add :<: f,Mul :<: f,Val :<: f) => Fix f
e = val 100 .+. val 20 .*. val 30 .+. val 100 .+. val 20 .*. val 30 .+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30.+. val 100 .+. val 20 .*. val 30



