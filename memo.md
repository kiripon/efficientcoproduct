{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}

data (f :+: g) e = Inl (f e) | Inr (g e)

class Corceable a b where
  corce :: a -> b

-- Expr = Add Expr Expr | Val Int | Hoge と Fix (Add :+: Val :+: Hoge)　という型があったとする。
-- これらは共に Add をコンストラクタに持っている。
-- この `Addをコンストラクタに持つ` という制約を型クラスで表現してやりたい。
-- 素直にやるならば以下のような制約が考えられる。
{-
class HasAdd a where
  add :: a -> a -> a
-}
-- この`add`をコンストラクタに持つことをモジュラーに表現してやりたい。
-- 考えられる方法として、型クラスを使わないでTHで表現する方法がある。

{-
''(Add :<: f)
-}

-- こうかいた時に以下のような制約が生成されてほしい
{-
class HasAdd a where
  add :: a -> a -> a
instance (Add :<: f) => HasAdd (Fix f) where
  add = inject . Add 

instance HasAdd Expr  where
  add = Add
-}

{-
6/17
<+> が多相的に使えるようにしたい。
Expr(合成後)
と
Expr(合成前)
とでどちらも同じ関数が使えるといい。そのために<+>を持つ型の型クラスを作りたい。
class HasAdd a where
  (<+>) :: a -> a -> a
しかしこれだとコンストラクタ毎に型クラスを作らなければならない。
できればモジュラーに書けるようにしたい。

instance Add :<: g where
-}


* ベンチマーク
* 原因がわからないといけない
* プロファイラを使って調べるとか
* 
